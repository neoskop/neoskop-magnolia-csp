package de.neoskop.magnolia.csp.helper;

import de.neoskop.magnolia.csp.CSPModule;
import info.magnolia.objectfactory.Components;

/**
 * @author Arne Diekmann
 * @since 08.01.18
 */
public class CSPHelper {
  public static String getCurrentNonce() {
    return Components.getComponent(CSPModule.class).getPlaceholderNonce();
  }
}
