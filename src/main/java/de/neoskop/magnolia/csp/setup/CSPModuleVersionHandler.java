package de.neoskop.magnolia.csp.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.FilterOrderingTask;
import info.magnolia.module.delta.IsAuthorInstanceDelegateTask;
import info.magnolia.module.delta.SetPropertyTask;
import info.magnolia.module.delta.Task;
import info.magnolia.repository.RepositoryConstants;
import java.util.Arrays;
import java.util.List;

/**
 * This class is optional and lets you manage the versions of your module, by registering "deltas"
 * to maintain the module's configuration, or other type of content. If you don't need this, simply
 * remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class CSPModuleVersionHandler extends DefaultModuleVersionHandler {
  private static final String CSP_FILTER_PATH = "/server/filters/csp";
  private static final String FILTER_NAME = "csp";

  @Override
  protected List<Task> getExtraInstallTasks(InstallContext installContext) {
    return Arrays.asList(
        new FilterOrderingTask(FILTER_NAME, new String[] {"range"}),
        new IsAuthorInstanceDelegateTask(
            "",
            new SetPropertyTask(RepositoryConstants.CONFIG, CSP_FILTER_PATH, "enabled", "false")));
  }
}
