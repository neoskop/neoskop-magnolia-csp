package de.neoskop.magnolia.csp;

import de.neoskop.magnolia.csp.model.CSPHeaderConfig;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.commons.codec.binary.Base64;

public class CSPModule implements ModuleLifecycle {
  public static final String CSP_NONCE = "csp-nonce";
  private List<CSPHeaderConfig> headerConfigs = new ArrayList<>();
  private String reportUri = "/.csp-report";
  private String placeholderNonce;

  public List<CSPHeaderConfig> getHeaderConfigs() {
    return headerConfigs;
  }

  public void setHeaderConfigs(List<CSPHeaderConfig> headerConfigs) {
    this.headerConfigs = headerConfigs;
  }

  public String getReportUri() {
    return reportUri;
  }

  public void setReportUri(String reportUri) {
    this.reportUri = reportUri;
  }

  public String getPlaceholderNonce() {
    return placeholderNonce;
  }

  @Override
  public void start(ModuleLifecycleContext moduleLifecycleContext) {
    final byte[] bytes = new byte[16];
    new Random().nextBytes(bytes);
    this.placeholderNonce = Base64.encodeBase64String(bytes);
  }

  @Override
  public void stop(ModuleLifecycleContext moduleLifecycleContext) {}
}
