package de.neoskop.magnolia.csp.filter;

import de.neoskop.magnolia.csp.CSPModule;
import de.neoskop.magnolia.csp.model.CSPHeaderConfig;
import de.neoskop.magnolia.csp.wrapper.HtmlResponseWrapper;
import info.magnolia.cms.filters.OncePerRequestAbstractMgnlFilter;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.site.ExtendedAggregationState;
import info.magnolia.objectfactory.Components;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 08.01.18
 */
public class CSPFilter extends OncePerRequestAbstractMgnlFilter {
  private static final Logger LOG = Logger.getLogger(CSPFilter.class);
  private static final String POST = "POST";

  @Override
  public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    final String domainName =
        ((ExtendedAggregationState) MgnlContext.getAggregationState()).getDomainName();

    final CSPModule module = Components.getComponent(CSPModule.class);

    if (request.getRequestURI().startsWith(module.getReportUri())
        && POST.equalsIgnoreCase(request.getMethod())) {
      handleCspReport(request, response, domainName);
      return;
    }

    final Optional<CSPHeaderConfig> configOptional =
        module
            .getHeaderConfigs()
            .stream()
            .filter(Objects::nonNull)
            .filter(
                c ->
                    StringUtils.isNotBlank(c.getDomain())
                        && c.getDomain().equalsIgnoreCase(domainName))
            .findFirst();

    if (configOptional.isPresent()) {
      final CSPHeaderConfig config = configOptional.get();
      final HtmlResponseWrapper wrappedResponse = new HtmlResponseWrapper(response);
      chain.doFilter(request, wrappedResponse);

      if (wrappedResponse.getContentType() != null
          && wrappedResponse.getContentType().contains("text/html")) {
        config.addToResponse(wrappedResponse);
      } else {
        response.getOutputStream().write(wrappedResponse.getCaptureAsBytes());
      }
    } else {
      chain.doFilter(request, response);
    }
  }

  private void handleCspReport(
      HttpServletRequest request, HttpServletResponse response, String domainName)
      throws IOException {
    StringBuilder buffer = new StringBuilder();
    BufferedReader reader = request.getReader();
    String line;

    while ((line = reader.readLine()) != null) {
      buffer.append(line);
    }

    LOG.warn("Received a CSP violation report for " + domainName + ": " + buffer.toString());
    response.setStatus(HttpServletResponse.SC_OK);
  }
}
