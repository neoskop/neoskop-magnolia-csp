package de.neoskop.magnolia.csp.model;

import de.neoskop.magnolia.csp.CSPModule;
import de.neoskop.magnolia.csp.wrapper.HtmlResponseWrapper;
import info.magnolia.objectfactory.Components;
import java.io.IOException;
import java.util.Random;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;

/**
 * @author Arne Diekmann
 * @since 08.01.18
 */
public class CSPHeaderConfig {
  private static final String CSP_HEADER_NAME = "Content-Security-Policy";
  private String header;
  private String domain;

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  private String generateRandomNonce() {
    final byte[] bytes = new byte[16];
    new Random().nextBytes(bytes);
    return Base64.encodeBase64String(bytes);
  }

  public void addToResponse(HtmlResponseWrapper response) throws IOException {
    String finalHeaderValue = header;
    final String nonce = generateRandomNonce();

    addResponseHeader(response, finalHeaderValue, nonce);
    replacePlaceholderInBody(response, nonce);
  }

  private void replacePlaceholderInBody(HtmlResponseWrapper response, String nonce)
      throws IOException {
    final String content = response.getCaptureAsString();
    final String placeholderNonce = Components.getComponent(CSPModule.class).getPlaceholderNonce();
    final String finalContent = content.replaceAll(Pattern.quote(placeholderNonce), nonce);
    response.getResponse().getWriter().write(finalContent);
  }

  private void addResponseHeader(
      HttpServletResponse response, String finalHeaderValue, String nonce) {
    if (header.contains("{nonce}")) {
      finalHeaderValue = header.replaceAll("\\{nonce}", nonce);
    }

    response.setHeader(CSP_HEADER_NAME, finalHeaderValue);
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }
}
