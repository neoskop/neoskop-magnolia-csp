# README

# Abhängigkeiten

- [Magnolia CMS][1] >= 5.3.7

# Installation

Als Repository muss [JitPack](https://jitpack.io/) in der `pom.xml` des Magnolia-Projektes hinzugefügt werden:

```xml
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Das Modul muss dann in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-csp</artifactId>
	<version>1.1.4</version>
</dependency>
```

[1]: https://www.magnolia-cms.com
